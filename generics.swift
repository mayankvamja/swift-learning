func swap<T>(_ a:inout T, _ b:inout T) { (a, b) = (b, a) }
var a = 0
var b = 1
print("\(type(of: &a))")
print("a = \(a), b = \(b)")
swap(&a, &b)
print("a = \(a), b = \(b)")

func foo<T> (_ a: T) -> Void {
  print("\(a)\n")
}

foo("xyz")

func sum<T: Numeric> (_ args:T ...) -> T {
  var sum: T = 0
  for i in args {
    sum = sum + i
  }
  return sum
}

print(sum(1.12, 123.23))
print()

func display <T: Sequence> (_ seq: T) -> Void {
  for i in seq {
    print("\(i)")
  }
}

display(1...5)
print()

display(["Hi", "Hello"])