extension Double {
  var kmph: Double { return self }
  var mph: Double { return self * 0.621371 }

  func kmToMile() -> Double {
    return self * 0.621371
  }

  func mileTokm() -> Double {
    return self / 0.621371
  }

  func toString() -> String {
    return String(self)
  }
}

var speed = 150.kmph
print(speed)
print(speed.mph)

var s: Double = 200.0
print(s.kmToMile())
print(s.mileTokm())
print(s.toString())

protocol Proto {
  func foo()
}

extension Proto {
  // var xyz : String {return "xyz"}
  func foo() {
    print("Foo")
  }
}

class Imp: Proto {}

var imp = Imp()
imp.foo()
print(imp.xyz)