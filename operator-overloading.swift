class Point<T: Numeric> {
  var x: T
  var y: T
  var z: T

  init(_ x: T, _ y: T, _ z: T) {
    self.x = x
    self.y = y
    self.z = z
  }

  func display() {
    print("X: \(x), Y: \(y), Z: \(z)")
  }

  static func + (p1: Point, p2: Point) -> Point {
    return Point(p1.x + p2.x, p1.y + p2.y, p1.z + p2.z)
  }

  static func == (p1: Point, p2: Point) -> Bool {
    if (p1.x == p2.x && p1.y == p2.y && p1.z == p2.z) {
      return true
    }
    return false
  }
}

var p1 = Point<Int>(1, 2, 3)
var p2 = Point<Int>(1, 2, 3)

p1.display()
p2.display()

var p3 = p1 + p2
p3.display()

if (p1 == p2) {
  print("Points are equal.")
} else {
  print("Points are not equal.")
}