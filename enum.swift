enum Day {
  case Sunday(String, Int)
  case Monday(String, Int)
  case Tuesday(String, Int)
  case Wednesday(String, Int)
  case Thursday(String, Int)
  case Friday(String, Int)
  case Saturday(String, Int)

  static var description: String = "Days Enumeration."

  func displayInfo() -> Void {
    if(self(let label, let num)) {
      print("SELF")
    }
    print(self)
  }

  var data: (String, Int) { 
    switch self {
      case .Sunday(let label, let num):
        return (label, num)
      case .Monday(let label, let num):
        return (label, num)
      case .Tuesday(let label, let num):
        return (label, num)
      case .Wednesday(let label, let num):
        return (label, num)
      case .Thursday(let label, let num):
        return (label, num)
      case .Friday(let label, let num):
        return (label, num)
      case .Saturday(let label, let num):
        return (label, num)
    }
  }
}

enum DayNumber: Int {
  case Sunday = 1, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
}

enum DayLabel: String {
  case Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
}

var currentDay: Day = .Monday(DayLabel.Monday.rawValue, DayNumber.Monday.rawValue)
print(type(of:currentDay))
print(currentDay.data.0)
print(currentDay.data.1)
print()

currentDay.displayInfo()
