var array1 = [Int]()
 
print("Empty \(array1.isEmpty)")
array1.append(5)
array1 += [7, 9]
print("Index 1 : \(array1[1])")
array1[0] = 4
array1.insert(10, at: 3)
array1.remove(at: 3)
array1[0...2] = [1,2,3]
print("Length : \(array1.count)")
// FILL WITH DEFAULT VALUE
var array2 = Array(repeating: 0, count: 5) 
var array3 = array1 + array2

for item in array3 {
  print(item)
}

// ITERATOR 
for (index, value) in array3.enumerated() {
  print("\(index) : \(value)")
}
 
// using range
var array4 = Array(1...6)
 
// slice
print("Array : \(array4[1...2])")
 
// Insert multiple
array4[1..<2] = [9, 8]
print("Array : \(array4)")
 
// Insert array but keep values
array4[1..<1] = [10, 11]
print("Array : \(array4)")
 
// print("111 Index : \(array4.firstIndex(of:111)! ?? 0)")
print("11? : \(array4.contains(11))")
print("Min : \(array4.min()!)")
print("Max : \(array4.max()!)")

array4.sort()
array4.sort{$0 > $1}

// 2D ARRAY 
var array5 = [[1,2,3], [4,5,6], [7,8,9]]
print("Array : \(array5[1][0])")
 
// Flatten an array of arrays
var array6 = Array(array5.joined())
print("Flatten : \(array6)")

print("Odds : \(array6.split{$0.isMultiple(of:2)})")
var array7 = array6.filter{$0.isMultiple(of:2)}
print(array7)
var array8 = array6.filter{$0>5}
print("Less then 12 : \(array6.allSatisfy({$0 < 12}))")
 
var array9 = array6.map{$0 * 2}
print(array8)
 
print("Sum : \(array9.reduce(0){$0 + $1})")