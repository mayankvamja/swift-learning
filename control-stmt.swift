var age: Int = 8
 
if age < 5 {
  print("Preschool")
} else if age == 5 {
  print("Kindergarten")
} else if (age > 5) && (age <= 18){
  let grade: Int = age - 5
  print("Grade \(grade)")
} else {
  print("College")
}
 
// Or logical operator
var income: Double = 12000
var tax: Double = 33.33
 
print("Get Grant : \((income < 15000) || (tax >= 33.88))")

print("")

// ! : NOT OPERATOR, &&, || : And-Or Logical 

var canDrive: Bool = age >= 16 ? true : false
 
// SWITCH
let ingredient = "pasta"
// No break needed
switch ingredient {
  case "tomatoes", "pasta":
    print("Spaghetti")
  case "beans":
    print("Burrito")
  case "potatoes":
    print("Mashed Potatoes")
  default:
    print("Water")
}
 
// match ranges
let score: Int = 89
        
switch score {
  case 93...100:
    print("A")
  case 85...92:
    print("B")
  case 77...84:
    print("C")
  case 69...76:
    print("D")
  default:
    print("F")
}