// FUNCTION
func sum(one: Int, two: Int) -> Any {
  return one + two
}

let res = sum(one: 10, two: 20)
// let res1 = sum(two: 10, one: 20) // INVALID
print("Sum of 10 & 20 is \(res) : \(type(of:res))\n")

// w/o label
func check(_ num: Int) -> Void {
  print("# NUM : \(num) : \(type(of: num))")
  let num = 10 // ???
  print("# NUM : \(num) : \(type(of: num))")  
}

check(1)

// Refs
var num : Int = 11

func checkWithRef(_ no : inout Int) -> Void {
  no += 5
  print("Checking Num : \(no)")
}

checkWithRef(&num)

// Overloading
func sum(_ num1 : Int, _ num2: Int) -> Int {
  return num1 + num2
}

func sum(_ num1: Double, _ num2: Double) -> Double {
  return num1 + num2
}

func sumOf(_ numbers : Int ...) -> Int {
  var sum = 0
  for num in numbers {
    sum += num
  }
  return sum
}

print(sum(1,2))
print(sumOf(1,2,3,4,5))
print(sum(1.1,2.2))

// CLOUSER
// (num : Int) -> Int in num * num
var square : (Int) -> Int = { num in num * num }

print("\nSQUARE OF 3 : \(square(3))")

var arr = [2,4,1,7]

print(arr.sorted(by: { (a: Int, b: Int) -> Bool in 
  return a > b
}))
print(arr.sorted(by: { $0 > $1 }))
print(arr.sorted(by: > ))
print(arr.sorted() { $0 > $1 })
print(arr.sorted{ $0 > $1 })

var isEqual: (Int, Int) -> Bool = (==)

print("")
print(isEqual(10, 10))
print(isEqual(10, 20))