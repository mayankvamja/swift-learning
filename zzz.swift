class ClassRoom {
  var students = [
    ["ABSAS", "RFNDS", "AYEWAS", "SAGDKA", "SDANS"],
    ["TWEDD", "IYWEG", "IJASVD", "AYSD", "ASDK"],
    ["MAKSA", "YEIA", "QWPE", "YQHWSDA", "OIWQOIE"],
    ["EWFNCIO", "VCJBS", "MLASMD", "NAJSN", "QOWJE"],
    ["OWSM", "WEOWEE", "UIEHRN", "ASBD", "NXCV"],
  ]

  subscript(row: Int, col: Int) -> String {
    get { 
      return students[row][col]
    }
    set {
      students[row][col] = newValue
    }
  }

  subscript(name: String) -> (Int, Int)? {
    for row in 0..<students.count {
      for col in 0..<students[row].count {
        if(students[row][col] == name) {
          return (row, col)
        }
      }
    }
    return nil
  }

}

var classroom = ClassRoom()
print("\(classroom[1, 2]) = \(classroom.students[1][2])")

classroom[1, 2] = "MAYANK"
print("\n\(classroom[1, 2]) = \(classroom.students[1][2])")

if let pos = classroom["MAYANK"] {
  print(pos)
} else {
  print("Student 'MAYANK' Not Found")
}

// print("\n\(ClassRoom[1, 2, 0])")