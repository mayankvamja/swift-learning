
var arrInt = [1,2,3]

// FOR LOOP
for item in arrInt {
  print(item)
}
 
// range
for i in 1...5 {
  print(i)
}
 
// WHERE CLAUSE
for i in 1...10 where i % 2 == 0 {
  print("Even Number : \(i)")
}
 
// Use stride
for i in stride(from: 10, through:2, by: -3){
  print(i)
}
 
// forEach : $0
arrInt.forEach{ print($0) }
 
// WHILE LOOP
var i: Int = 1
while i < 10 {
  if i % 2 == 0 {
    i += 1
    continue
  }
  if i == 7 {
    break
  }
  print(i)
  i += 1
}
 
// REPEAT WHILE
let magicNum: Int = Int.random(in: 1...10)
var guess: Int = 0

repeat {
  print("Guess : \(guess)")
  guess += 1
} while (magicNum != guess)
 
print("Magic Number was \(magicNum)")
 
// Create an iterator : next()
var i1 = (1...5).makeIterator()
while let i = i1.next(){
  print(i)
}
