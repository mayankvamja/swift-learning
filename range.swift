// a...z : Everything from a to z including z
let r1 = 1...3
print("TYPE r1 : \(type(of: r1))")
for i in r1{
    print(i)
}

let r2 = 1..<3
print("\nTYPE r1 : \(type(of: r2))")
for i in r2{
    print(i)
}

print("\n")
for i in 5...10{
    print(i)
}

print("\n")

// Loop in reverse
for i in (5...10).reversed(){
    print(i)
}

print("\n")

// containes
print("5 in range : \((1...5).contains(5))")
 