import Foundation

print("Hello World")

// VARIABLES
let intConst = 0
// intConst = 5
print("\n\(type(of: intConst)) Const = \(intConst)")

var intVar = 5
intVar += 5
print("\(type(of: intVar)) Var   = \(intVar)")

// Min and max values
print("Min Int \(Int64.min)")
print("Max Int \(Int64.max)")
 
// Floats and Doubles store numbers with decimals
var pi2: Float = 3.1415
var pi3: Double = 3.1415
 
// Min and max doubles
print("Min Double \(Double.leastNormalMagnitude)")
print("Max Double \(Double.greatestFiniteMagnitude)")
 
// Floats : 15 digits
var bigF = 1.1111111111111111
var bigF2 = 1.1111111111111111
print("Big Float : \(bigF + bigF2)")
 
// Booleans
let canVote: Bool = true
 
// character
var myGrade: Character = "A"
 
// Casting changes a value from 1 type to another
print("Dbl to Int : \(Int(3.4))")
print("Int to Dbl : \(Double(3))")
 
// Other data types : Int8, Int16, Int32, Int64,
// UInt8, UInt16, UInt32, UInt64, Float80 
 
// Generate a random value from 1 to 10
var rand = Int.random(in: 1...10)
print("Rand : \(rand)")
 
// Math Functions
print("\nabs(-5) = \(abs(-5))")
print("floor(5.5) = \(floor(5.5))")
print("ceil(5.4) = \(ceil(5.4))")
print("round(5.4) = \(round(5.4))")
print("max(5,4, 9) = \(max(5,4, 9))")
print("min(5,4) = \(min(5,4))")
print("pow(5,2) = \(pow(5.0,2))")
print("sqrt(25) = \(sqrt(25))")
print("log(12.213) = \(log(12.213))")

var arr : [Int] = [1,2,3]
arr.append(4)
arr += [5,6]

print("\(arr)")


/*
control-stmt.swift
loops
fuunctions
rage
array
*/