 
enum DivisionError: Error{
  case DivideByZero
}
 
func divide(_ num1: Float, _ num2: Float) throws -> Float {
  guard num2 != 0.0 else {
    throw DivisionError.DivideByZero
  }
  return num1/num2
}
 
do {
  let res = try divide(4.0, 0.0)
  print(res)
} catch DivisionError.DivideByZero {
  print("Can't Divide by Zero")
}

print("Hello")