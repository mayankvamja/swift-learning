// import Vehicle
// import VehicleColor
// import VehicleTransmission

class HeroSplender: Vehicle {
  var color: VehicleColor
  var maxSpeed: Double
  var milage: Double

  init(_ color: VehicleColor = .Black, _ maxSpeed: Double = 90.0, _ milage: Double = 65.0) {
    self.color = color
    self.maxSpeed = maxSpeed
    self.milage = milage
  }

  func switchOn() -> Void {
    print("Switched on Hero Splender.")
  }
  func switchOff() -> Void {
    print("Switched off Hero Splender.")
  }

  func getTransmission() -> VehicleTransmission {
    return .IMT
  }

  func getSpecifications() { 
    print("# Hyundai Verna Specifications #")
    print("Color     : \(self.color.rawValue)")
    print("Max-speed : \(self.maxSpeed)")
    print("Milage    : \(self.milage)")
  }
}

var bike = HeroSplender()
bike.getSpecifications()