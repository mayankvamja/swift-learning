open enum VehicleColor: String {
  case White, Black, Red, Blue, Yellow, Gray, Gold, Orange 
}

open enum VehicleTransmission: String {
  case Manual, AMT, IMT
}

open protocol Vehicle {
  var color: CarColor { get }
  var maxSpeed: Double { get }
  var milage: Double { get }

  func switchOn() -> Void
  func switchOff() -> Void

  func getTransmission() -> CarTransmission
}