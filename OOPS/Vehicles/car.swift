enum CarColor: String {
  case White, Black, Red, Blue, Yellow, Gray, Gold, Orange 
}

enum CarTransmission: String {
  case Manual, AMT, IMT
}

protocol CarDataSource {
  var color: CarColor { get }
  var maxSpeed: Double { get }
  var milage: Double { get }

  func switchOn() -> Void
  func switchOff() -> Void

  func getTransmission() -> CarTransmission
}

class HyundaiVerna: CarDataSource {
  var color: CarColor
  var maxSpeed: Double
  var milage: Double

  init(_ color: CarColor = .White, _ maxSpeed: Double = 200.0, _ milage: Double = 18.0) {
    self.color = color
    self.maxSpeed = maxSpeed
    self.milage = milage
  }

  func switchOn() -> Void {
    print("Switched on Hyundai Car.")
  }
  func switchOff() -> Void {
    print("Switched off Hyundai Car.")
  }

  func getTransmission() -> CarTransmission {
    return .IMT
  }

  func getSpecifications() { 
    print("# Hyundai Verna Specifications #")
    print("Color     : \(self.color.rawValue)")
    print("Max-speed : \(self.maxSpeed)")
    print("Milage    : \(self.milage)")
  }
}

var verna = HyundaiVerna()
verna.maxSpeed = 300.0
verna.getSpecifications()