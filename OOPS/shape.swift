private class Shape {
  var name: String = "Shape"
  var description: String = "It is a simple shape."

  init() {}
  init(name: String, description: String) {
    self.name = name
    self.description = description
  }

  deinit {
    print("Shape Deinitialized.")
  }

  fileprivate func area() -> Double { 
    return 0.0
  }

  final func getInfo() -> Void {
    print("""
    Shape Name : \(self.name)
    Description: \(self.description)
    Area       : \(self.area())
    """)
  }
}

var s = SHape()
s.area()

// class XYZ {
//   var shape: Shape = Shape()
// }

// var shape1 = Shape()
// let a: Any = 12
// shape1.abc(12 as AnyObject)
// var xyz = XYZ()
// xyz.shape.getInfo()

private class Square: Shape {
  var length : Double = 0.0

  init(_ length: Double, name: String = "Square", description: String = "Square type of shape with same height and width.") {
    super.init(name: name, description: description)
    self.length = length
  }

  override func area() -> Double {
    return length * length
  }

  deinit {
    print("Square Deinitialized.")
  }
}

private var shape: Shape = Shape()
shape.getInfo()
print("\nTYPE : \(type(of: shape))\n")

shape = Square(10)
shape.getInfo()
print("\nTYPE : \(type(of: shape))\n")