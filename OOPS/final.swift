import Foundation

class UseFinal {
  final var val: Int = 1
  func foo() {
    val = 22
    // self.val = 2
    // print(self.val)
  }
}

var f = UseFinal()
f.foo()
print(f.val)

class NoUseFinal {
  private var val: Int = 1
  func foo() {}
}

// var start = Date()
// for _ in 1...100000 {
//   let temp = NoUseFinal()
//   temp.foo()
// }
// print("noUseFinal took \(Date().timeIntervalSince(start))")

// var start2 = Date()
// for _ in 1...100000 {
//   let temp = UseFinal()
//   temp.foo()
// }

// print("useFinal took   \(Date().timeIntervalSince(start2))")