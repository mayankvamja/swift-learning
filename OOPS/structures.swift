struct Rectangle {
  // properties
  var height : Double = 0.0
  var length : Double = 0.0
  
  // methods
  func area() -> Double { return height * length }
}

// Create a Rectangle
let myRect = Rectangle(height: 10.0, length: 15.0)
 
print("Rect-Area : \(myRect.height) * \(myRect.length) = \(myRect.area())")
