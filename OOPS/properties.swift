struct Cuboid {
  var width = 0.0 {
    willSet {
      print("Width will set to \(newValue)")
    }
    didSet {
      print("Width changed from \(oldValue) t0 \(width)")
    }
  }
  var height = 0.0, depth = 0.0
  var volume: Double {
    return width * height * depth
  }
}
var cub = Cuboid(width: 4.0, height: 5.0, depth: 2.0)
print("the volume of fourByFiveByTwo is \(cub.volume)")
cub.width = 5
print("the volume of fiveByFiveByTwo is \(cub.volume)")

// PROPERTY WRAPPER


@propertyWrapper
struct CamelCaps {
  private var str: String
  init() { self.str = "" }

  var wrappedValue: String {
    get {
      return str
    }
    set {
        self.str = newValue.split(separator: " ").map({
            $0.prefix(1).uppercased() + $0.dropFirst()
        }).joined(separator: " ")
    }
  }
}

class CapsExample {
  @CamelCaps var str: String
  init() {
      self.str = "hey there! hello world"
  }
}

var cap = CapsExample()

print(cap.str)