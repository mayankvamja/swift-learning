var any: Any? = nil
print("Type of 'any' is \(type(of: any))")

var x: Any = "X"
// var y: AnyObject = "Y" // INVALID
var y: AnyObject = "Y" as AnyObject

print("Type of \(x) is \(type(of: x)) : \(x is String)")
print("Type of \(y) is \(type(of: y)) : \(y is String)")

class ABC {}

var abc: ABC = ABC()
print("\nabc : \(type(of: abc))")
// abc = x // ERROR
// abc = y // ERROR - abc: ABC
// x = ABC()
// abc = x as! ABC
// print("abc : \(type(of: abc))")
// y = ABC()
// abc = y as! ABC
// print("abc : \(type(of: abc))")

var abc1: AnyObject = ABC()
print("\nabc1 : \(type(of: abc1))")
// abc1 = x // ERROR - abc1: AnyObject, x: Any
// x = ABC()
// abc1 = x as AnyObject
// print("abc : \(type(of: abc1))")
abc1 = y
print("abc1 : \(type(of: abc1))")

var abc2: Any = ABC()
print("\nabc2 : \(type(of: abc2))")
abc2 = x
print("abc2 : \(type(of: abc2))")
abc2 = y
print("abc2 : \(type(of: abc2))")
// abc2 = nil // 'nil' cannot be assigned to type 'Any'