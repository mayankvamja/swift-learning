// Clusure as func arg
func getData(from url: String, onData: ([String : String]) -> Void) {

  print("\nLoading Data from \(url)")

  if(url == "http://api.data.com") {
    onData(["data": "Hello There", "success": "true"])
  } else {
    print("Failed to get data")
  }
}

let printResult: (_ res: [String: String]) -> Void = { res in
  print("\(res["data"]!)")
  print("\(res["success"]!)")
}

getData(from: "http://api.dataaa.com", onData: { res in
  printResult(res)
})

getData(from: "http://api.data.com") { res in
  printResult(res)
}

getData(from: "http://api.data.com") { printResult($0) }
getData(from: "http://api.data.com", onData: printResult)


// AUTOCLOSURE for code clean (readability may reduce)
func greetGoodMorning(isMorning: Bool, name: @autoclosure () -> String) {
  print("\nGreetGoodMorning called")
  if isMorning {
    print("Good Morning \(name())")
  }
}

func getName(_ name: String) -> String {
  print("Get Name Called")
  var arr = [String]()
  for _ in 0...8000000 { arr.append(name) }
  return arr.last!
}

greetGoodMorning(isMorning: true, name: getName("Mayank"))

// ESCAPING CLOSURE - to let closure execute in other scope than func itself
func greetGoodNoon(isNoon: Bool, name: @autoclosure @escaping () -> String) {
  print("\nGreetNoon called")
  if isNoon {
    name()
    func foo() {
      print("Good Afternoon \(name())")
    }
    foo()
  }
  print("GreetNoon over!\n")
}

greetGoodNoon(isNoon: true, name: getName("Mayank"))