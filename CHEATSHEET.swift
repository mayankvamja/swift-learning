/* BASIC OPERATORS : 
  var - Normal
  let - Constants  */
var iInt    : Int    = 0
let fFloat  : Float  = 0.0000000001 // 15 digits after .
var dDouble : Double = 11231.123123
var sString : String = "Hello"      // """ MULTILINE """
var isBool  : Bool   = true

/* Other data types : Int8, Int16, Int32, Int64,
   UInt8, UInt16, UInt32, UInt64, Float80   */

// All DataTypes are struct
// Can be re-initialized for 
/* Type casting */
iInt    = Int("10")       = Int(dDouble)
dDouble = Double("10.10") = Double(iInt)
sString = String(iInt)    = String(fFloat)

/* OPERATORS 
Mathematical: +, -, *, /, %
Assignment  : =,  +=, -=, *=, /=, %=
Not         : ! (!true == false)
Relatopnal  : ==, !=, >=, <=, <, > ---> Bool
 === : Checks if pointing to same reference
 !== : Checks if don't point at same reference
Logical     : && (and), || (or)
*/
var a: Float = 1.0
a = a + 2.2
a -= 2.2
let isAOne = a == 1.0 // true

/* Math Functions */
print("abs(-5) = \(abs(-5))")
print("floor(5.5) = \(floor(5.5))")
print("ceil(5.4) = \(ceil(5.4))")
print("round(5.4) = \(round(5.4))")
print("max(5,4) = \(max(5,4))")
print("min(5,4) = \(min(5,4))")
print("pow(5,2) = \(pow(5,2))")
print("sqrt(25) = \(sqrt(25))")
print("log(2.71828) = \(log(2.71828))")
 
// There is also sin, cos, tan, asin, acos, atan
// sinh, cosh, tanh

/* Control Statements */
 
// *****IF / ELSE
var age: Int = 8
 
if age < 5 {
  print("Preschool")
} else if age == 5 {
  print("Kindergarten")
} else if (age > 5) && (age <= 18){
  let grade: Int = age - 5
  print("Grade \(grade)")
} else {
  print("College")
}

// *****Turnary : ?
var gradeFromAge = age < 5 ? "Preschool" 
                    : age == 5 ? "Kindergarten"
                    : (age > 5) && (age <= 18) ? "Grade \(age - 5)"
                    : "College"

// *****SWITCH
// break -------> to break out from switch
// through -----> to go through next cases
let ingredient = "pasta"

switch ingredient {
  // No break needed if min one stmt is There -- autobreak
  case "tomatoes", "pasta":
    print("Spaghetti")
  case "beans":
    print("Burrito")
  case "potatoes":
    print("Mashed Potatoes")
  default:
    print("Water")
}
 
// match ranges
let score: Int = 89
        
switch score {
  case 93...100:
    print("A")
  case 85...92:
    print("B")
  case 77...84:
    print("C")
  case 69...76:
    print("D")
  default:
    print("F")
}


// ***** LOOPING
var arrInt = [1,2,3]

// FOR LOOP
for item in arrInt {
  print(item)
}
 
// range
for i in 1...5 {
  print(i)
}
 
// WHERE CLAUSE
for i in 1...10 where i % 2 == 0 {
  print("Even Number : \(i)")
}
 
// Use stride
for i in stride(from: 10, through:2, by: -3){
  print(i)
}
 
// forEach : $0 refers to element in array
arrInt.forEach{ print($0) }
 
// WHILE LOOP
var i: Int = 1
while i < 10 {
  if i % 2 == 0 {
    i += 1
    continue // TO CONTINUE IN LOOP
  }
  if i == 7 {
    break    // BREAK FROM LOOP
  }
  print(i)
  i += 1
}
 
// REPEAT WHILE --- min one time execution
let magicNum: Int = Int.random(in: 1...10)
var guess: Int = 0

repeat {
  print("Guess : \(guess)")
  guess += 1
} while (magicNum != guess)
 
print("Magic Number was \(magicNum)")
 
// Create an iterator : next()
var i1 = (1...5).makeIterator()
while let i = i1.next(){
  print(i)
}


// ***** Optional
// When value can be 'nil' for any type
var opt1: Int? = nil
opt1 = 123

// Unwrapping optionals

// 1. Using if or guard
if let opt1 = opt1 {
  print(opt1) // opt1 is now unwrapped and has value
} else { print("opt1 is nil") }

// 2. Using ! operator
print(opt1!)  // ! at the end to force-unwrap opt1 value from optional

// 3. Nil-Coalescing Operator ??
print(opt1 ?? "opt1 in nil")

var opt2: Int! = nil
opt2 = 1234
// Implicitly Unwrapped Oprional (IUO)  
// Force-unwrapped variable can also be nil
// https://www.hackingwithswift.com/quick-start/understanding-swift/why-does-swift-need-both-implicitly-unwrapped-optionals-and-regular-optionals
print(opt2) // to be used if you know about value's nullability


// **** Functions (arg1, arg2, ...) -> ReturnType
func sum(one: Int, two: Int) -> Int {
  return one + two
}

let res = sum(one: 10, two: 20)
// let res1 = sum(two: 10, one: 20) // INVALID
print("Sum of 10 & 20 is \(res) : \(type(of:res))\n")

// w/o label
func check(_ num: Int) -> Void {
  print("# NUM : \(num) : \(type(of: num))")
  let num = 10 // ???
  print("# NUM : \(num) : \(type(of: num))")  
}

check(1)

// inout variables as argument --- not let --- can be changed inside
// Reference when calling func --- using & --- allow an arg as inout
var num : Int = 11

func checkWithRef(_ no : inout Int) -> Void {
  no += 5
  print("Checking Num : \(no)")
}
checkWithRef(&num)

// Overloading
func sum(_ num1 : Int, _ num2: Int) -> Int {
  return num1 + num2
}

func sum(_ num1: Double, _ num2: Double) -> Double {
  return num1 + num2
}

// '...' for range of arguments (not range as argument)
// Known as Variadic Function
func sumOf(_ numbers : Int ...) -> Int {
  var sum = 0
  for num in numbers {
    sum += num
  }
  return sum
}

print(sum(1,2))
print(sumOf(1,2,3,4,5))
print(sum(1.1,2.2))

// ***** Closures
// (num : Int) -> Int in num * num
var square : (Int) -> Int = { num in num * num }

print("\nSQUARE OF 3 : \(square(3))")

var arr = [2,4,1,7]

print(arr.sorted(by: { (a: Int, b: Int) -> Bool in 
  return a > b
}))
// $0, $1, $2... work as arguments in order of closure argument
// Useful when you know number of arguments in closure
// Especially when there is 1 or 2 arguments
print(arr.sorted(by: { $0 > $1 }))
print(arr.sorted(by: > ))
print(arr.sorted() { $0 > $1 })
print(arr.sorted{ $0 > $1 })
// ALL ABOVE print same

var isEqual: (Int, Int) -> Bool = (==)

print("")
print(isEqual(10, 10))
print(isEqual(10, 20))

// Clusure as func arg
func getData(from url: String, onData: ([String : String]) -> Void) {

  print("\nLoading Data from \(url)")

  if(url == "http://api.data.com") {
    onData(["data": "Hello There", "success": "true"])
  } else {
    print("Failed to get data")
  }
}

let printResult: (_ res: [String: String]) -> Void = { res in
  print("\(res["data"]!)")
  print("\(res["success"]!)")
}

getData(from: "http://api.dataaa.com", onData: { res in
  printResult(res)
})
getData(from: "http://api.data.com") { res in
  printResult(res)
}
getData(from: "http://api.data.com") { printResult($0) }
getData(from: "http://api.data.com", onData: printResult)
// ALL ABOVE are same
/* Suppose getData has onData, onFailure closures --- works in swift 5.3
 * getData("http://randomurl") { res in
 *    // on data code here
 * } onFailure : { err in
 *    // on failure code here
 * }
 */

// AUTOCLOSURE for code clean (readability may reduce)
func greetGoodMorning(isMorning: Bool, name: @autoclosure () -> String) {
  print("\nGreetGoodMorning called")
  if isMorning {
    print("Good Morning \(name())")
  }
}

func getName(_ name: String) -> String {
  print("Get Name Called")
  var arr = [String]()
  for _ in 0...8000000 { arr.append(name) }
  return arr.last!
}

greetGoodMorning(isMorning: true, name: getName("Mayank"))

// ESCAPING CLOSURE - to let closure execute in other scope than func itself
func greetGoodNoon(isNoon: Bool, name: @autoclosure @escaping () -> String) {
  print("\nGreetNoon called")
  if isNoon {
    name()
    func foo() {
      print("Good Afternoon \(name())")
    }
    foo()
  }
  print("GreetNoon over!\n")
}

greetGoodNoon(isNoon: true, name: getName("Mayank"))


// ***** RANGE
// Ranges have a start and ending value
// a...c : Everything from a to c including c
let r1 = 1...3
for i in r1{
  print(i)
}
// a..<c : Everything from a to c, but not c 
let r2 = 1..<3
for i in r2{
  print(i)
}

// Combine ranges with for loops
for i in 5...10{
  print(i)
}

// Loop in reverse
for i in (5...10).reversed(){
  print(i)
}

// Check if value is in a range
print("5 in range : \((1...5).contains(5))")

// Range Types
// ClosedRange --> 1...5
// Half-Open   --> 1..<5
// One-Sided   --> 1...  = 1 to infinite

// ***** ARRAY
// Make empty array
var array1: [Int] = [Int]()
 
// Check if array is empty
print("Empty \(array1.isEmpty)")
 
// Add value to array
array1.append(5)
 
// Add another item
array1 += [7, 9]
 
// Get array item
print("Index 1 : \(array1[1])")
 
// Change value at index
array1[0] = 4
 
// Insert at an index
array1.insert(10, at: 3)
 
// Remove item
array1.remove(at: 3)
 
// Change multiple values
array1[0...2] = [1,2,3]
 
// Length of array
print("Length : \(array1.count)")
 
// Fill array with a value
var array2 = Array(repeating: 0, count: 5)
 
// Combine arrays
var array3 = array1 + array2
 
// Iterate through an array
for item in array3 {
  print(item)
}
 
// Get index and value
for (index, value) in array3.enumerated() {
  print("\(index) : \(value)")
}
 
// Create an array using a range
var array4 = Array(1...6)
 
// Get a slice of an array
print("Array : \(array4[1...2])")
 
// Insert multiple values at an index ==== replace(subrange)
array4[1..<2] = [9, 8]
print("Array : \(array4)")
 
// Insert array but keep values ==== insert(element , at:Index)
array4[1..<1] = [10, 11]
print("Array : \(array4)")
 
// Get index of value
print("11 Index : \(array4.firstIndex(of:11)!)")
print("11 Index : \(array4.lastIndex(of:11)!)")

// Check if array contains value ---> true/false
print("11? : \(array4.contains(11))")

// Get min and max value ---> For Numeric types
print("Min : \(array4.min()!)")
print("Max : \(array4.max()!)")
 
// Sort array
array4.sort()
array4.sort{$0 > $1}  // reverse
 
// Create an array of arrays
var array5 = [[1,2,3], [4,5,6], [7,8,9]]
print("Array : \(array5[1][0])")
 
// Flatten an array of arrays
var array6 = Array(array5.joined())
 
// Get odds only
print("Odds : \(array6.split{$0.isMultiple(of:2)})")
 
// Get evens
var array7 = array6.filter{$0.isMultiple(of:2)}
print(array7)
 
// Only keep values greater than 5
var array8 = array6.filter{$0>5}
 
// Check if values satisfy a condition
print("Less then 12 : \(array6.allSatisfy({$0 < 12}))")
 
// Create a new array by applying a function on each value
var array9 = array6.map{$0 * 2}
print(array8)
 
// Add all values in array - reduce (Getting single value from array manipulation)
print("Sum : \(array9.reduce(0){$0 + $1})")

/* EXTRA ARRAY PROPERTIES, METHODS
 * arr.first!  arr.last!
 * arr.removeFirst() -> Element, arr.removeFirst(Int)
 * arr.popLast() -> Element?, arr.removeLast() -> Element, arr.removeLast(Int)
 * arr.removeAll(where: (Element) -> Bool)
 * arr.contains(where: (Element) -> Bool)
 * Excluding: arr.dropFirst(Int), arr.dropLast(Int), arr.drop(where:)
 * Returns array: arr.sorted(by:) arr.reversed(), arr.shuffled()
 */


// ***** DICTIONARIES 
// A dictionary is an unordered list of key value pairs
 
// Create empty dictionary
var dict1 = [Int: String]()
 
// Check if empty
print("Empty : \(dict1.isEmpty)")
 
// Create an item with index of 1
dict1[1] = "Paul Smith"
 
// Create a dictionary with a string key
var cust: [String: String] = ["1": "Sally Marks", "2": "Paul Marks"]
 
// Size of dictionary
print("Size : \(cust.count)")
 
// Add an item
cust["3"] = "Doug Holmes"
cust["4"] = "Roug Holmes"
 
// Change a value
cust["3"] = "Doug Marks"
if let oldValue = cust.updateValue("Roug Marks", forKey: "4") {
  print("The old value is \(oldValue).")
} else {
  print("The dictionary does not contain a value for '4'.")
}

// Get a value
if let name = cust["3"] {
  print("Index 3 : \(name)")
}
 
// Remove a key value pair
cust["3"] = nil
if let removedValue = cust.removeValue(forKey: "4") {
  print("The removed value is \(removedValue).")
} else {
  print("The dictionary does not contain a value for '4'.")
}
 
// Iterate through a dictionary
for (key, value) in cust {
  print("\(key) : \(value)")
}

// EXTRA FOR DICTIONARIES
/* first!, randomElement() ---> (key, vakue)?
 * .map, .filter, .reduce, .contains 
 */


// ***** SETS
// Sets are an unordered list of unique elements
// Create an empty set
var sNums = Set<Int>()
 
// Add value to set
sNums.insert(1)
 
// Create set with an array
var sNums2: Set<Int> = [1, 2, 3]
 
// Values in set
print("Count : \(sNums2.count)")
 
// Check if empty
print("Empty : \(sNums2.isEmpty)")
 
// Remove a value
sNums2.remove(3)
 
// Check for a value
print("2 : \(sNums2.contains(2))")
 
// Add more values
sNums2.insert(5)
 
// Iterate a set
for i in sNums2 {
  print(i)
}


// ***** TUPLES
// Tuples can contain multiple values of different types
// Define a tuple with an Int and String
var t1 : (String, Int)
 
// Assign values
t1 = ("age", 45)
 
// Do the above on 1 line
var t2 : (String, Int) = ("age", 35)
 
// Assign values from a tuple
var sAge: String
var iAge: Int
(sAge, iAge) = t2
print(sAge)
 
// Get values by index
print("Tuple 0 : \(t2.0)")
 
// Change values
t2.0 = "money"

// You can assign labels to elements
var t3 : (city:String, state:String) = ("Ida", "OH")
print("Tuple State : \(t3.state)")

// ***** STRINGS
// Strings contain text and escape characters like
// \n : Newlines
// \t : Tabs
// \" : Escaped Double Quotes
// \' : Escaped Single Quotes
// \\ : Escaped Backslashes

var str3 = "A string\n"

// Multiline string
var str4 = """
Multiline
String
"""

// String interpolation
var num2 = 1
print("Your #\(num2)")

// Combine strings
var str5 = "a random string " + "and here is another"

// Check if a string is empty
print("String Empty : \(str5.isEmpty)")
 
// Get number of characters
print("String Size : \(str5.count)")

// Get the first character
print("First : \(str5[str5.startIndex])")

// Get the 5th character by offsetting from the 1st index
// You can also use endIndex
let index5 = str5.index(str5.startIndex, offsetBy: 5)
print("5th : \(str5[index5])")

// str5.contains("s") returns true if the string contains s
print("s in String : \(str5.contains("s"))")

// You can check for multiple values
print("Vowels in String : \(str5.contains{"aeiou".contains($0)})")

// Leave only characters in the list
print("Only Vowels : \("Derek".filter{"aeiou".contains($0)})")

// Get the 1st 4 characters
print("1st 4 : \(String(str5.prefix(4)))")

// Split converts strings to an array using a space as a separator
// An array contains multiple values
let arr = str5.split{$0 == " "}
// This for loop cycles through values in the array
for i in arr{
    print(i)
}

// Remove a character at the 1st index
str5.remove(at: str5.startIndex)

// Insert a character at index 0
str5.insert("A", at: str5.startIndex)
print(str5)

// Insert a string at index 15
let index6 = str5.index(str5.startIndex, offsetBy: 15)
str5.insert(contentsOf: " is great", at: index6)
print(str5)

// Get a string at defined indexes
let index7 = str5.index(str5.startIndex, offsetBy: 23)
print("String : \(str5[index6...index7])")

// Replace a string in a range
let r3 = index6...index7
str5.replaceSubrange(r3, with: " is super")
print(str5)

// Delete a string in a range
str5.removeSubrange(r3)
print(str5)

// Capitalize
print("Capitalize : \("abc".capitalized)")


// ***** ENUMERATIONS
// enum w/o rawValue
enum Emotion {
  case joy
  case anger
  case fear
  case disgust
  case sad
}

// Int rawValue starting from 1 to ... will be assigned.
enum DayNumber: Int {
  case Sunday = 1, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
}

// String rawValue will be assigned as per casse name if not assigned.
enum DayLabel: String {
  case Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
}

// enum with associatedValues
enum Day {
  case Sunday(String, Int)
  case Monday(String, Int)
  case Tuesday(String, Int)
  case Wednesday(String, Int)
  case Thursday(String, Int)
  case Friday(String, Int)
  case Saturday(String, Int)

  // static enum properties
  static var description: String = "Days Enumeration."

  // enum method
  func displayInfo() -> Void {
    if(self(let label, let num)) {
      print("SELF")
    }
    print(self)
  }

  // enum computed properties, stored properties not allowed
  var data: (String, Int) { 
    switch self {
      case .Sunday(let label, let num):
        return (label, num)
      case .Monday(let label, let num):
        return (label, num)
      case .Tuesday(let label, let num):
        return (label, num)
      case .Wednesday(let label, let num):
        return (label, num)
      case .Thursday(let label, let num):
        return (label, num)
      case .Friday(let label, let num):
        return (label, num)
      case .Saturday(let label, let num):
        return (label, num)
    }
  }
}

var currentDay: Day = .Monday(DayLabel.Monday.rawValue, DayNumber.Monday.rawValue)
print(type(of:currentDay))
print(currentDay.data.0)
print(currentDay.data.1)
print()

currentDay.displayInfo()


// *****EXCEPTIONS
// Define our error by defining a type of the Error protocol
enum DivisionError: Error{
  case DivideByZero
}

// Define we want the error to get thrown from the function
func divide(num1: Float, num2: Float) throws -> Float {
  guard num2 != 0.0 else {
    throw DivisionError.DivideByZero
  }
  return num1/num2
}
 
// Wrap code that could trigger an error in a do catch block
// catch the error and handle it
do {
  // try keyword before expression that may throw error
  try divide(num1: 4, num2: 0)
} catch DivisionError.DivideByZero {
  print("Can't Divide by Zero")
} catch {
  print("Unexpected Error occured.")
}


// **** STRUCTS ---- Value Type
// A struct is an object type that is used every place in
// Swift including Strings, Ints, Range and way more
 
// Create a rectangle struct
struct Rectangle {
  // It can contain properties
  var height = 0.0
  var length = 0.0
  
  // You can include methods
  func area() -> Double{
    let area = height * length
    return area
  }

  // You can't change properties for var type struct variable inside function
  // due to immutability in struct and enums
  // writing mutating keyword will make 'self' mutable.
  mutating func changeData(_ newHeight: Double, _ newLength: Double) -> Void {
    height = newHeight
    length = newLength
  }
}
 
// Create a Rectangle
let myRect = Rectangle(height: 10.0, length: 5.0)
// myRect.changeData(20.0, 10.0) will not work as myRect is let.
// Bit if Rectangle was class and myRect be still let then it will
// not affect instance properties or methods. So classes does not require mutating func.
print("Area : \(myRect.height) * \(myRect.length) = \(myRect.area())")


// ***** CLASSES ---- Reference Type
// Classes differ from structs in one main way being that
// classes can inherit from other classes.
// and classes are reference type while structs are value type
// which means struct1 = struct2 will create copies of struct
// where as obj1 = obj2 will create two refs for same object.

class ABCD {
    // Properties
    var name: String {
        return "ABCD"
    } 
    var abcd: String = ""
    
    // Initializers - struct, class
    init() {
        print("Init of ABCD")
    }
    
    // convenience Initializer
    convenience init(_ a: String) {
        // self refers to current object
        self.init() // required to call any self.init(...)
        self.abcd = a
    }
    
    // Deinitializer w/o params
    deinit {
        print("Deinitialized the object")
    }
    
    // methods
    func foo() {
        print("Foo \(self.name)")
    }
}

// Inhertance : Single, Multilevel supported by class
// Not supported by struct
// Use Protocols for it
class PQRS: ABCD {
    // override computed property
    override var name: String {
        return "PQRS"
    }
    var pqrs: String = ""

    override init() {
        // super.init()
        print("Init of PQRS")
        self.pqrs = "Hello PQRS"
    }

    // override method
    override func foo() {
        // can call super.foo() for super class method
        print("Foo \(self.name)")
    }   
}

var x: ABCD = ABCD("Hello ABCD")
print(x.name)
print(x.abcd)

x = PQRS()
print(x.name)

// I'll create a Warrior type and then we'll have a battle to the death
class Warrior{
  // These are the warriors properties
  var name: String = "Warrior"
  var health: Int = 100
  var attkMax: Int = 10
  var blockMax: Int = 10
  
  // Called every time you create a Warrior object
  init(_ name: String, _ health: Int, _ attkMax: Int, _ blockMax: Int){
    // self is used to refer to properties of an
    // object when we don't know the objects name
    self.name = name
    self.health = health
    self.attkMax = attkMax
    self.blockMax = blockMax
  }
  
  // Randomly calculates an attack amount
  func attack() -> Int{
    return Int.random(in: 1...self.attkMax)
  }
  // Randomly calculates block amount
  func block() -> Int{
    return Int.random(in: 1...self.blockMax)
  }
}
 
// The battle class is used to loop until a warrior dies
class Battle{
  func startFight(_ warrior1: Warrior, _ warrior2: Warrior){
    // Loops until a warrior dies giving each warrior
    // a chance to attack
    while true{
        if Battle.getAttkResult(warrior1, warrior2) == "Game Over"{
            print("Game Over")
            break
        }
        if Battle.getAttkResult(warrior2, warrior1) == "Game Over"{
            print("Game Over")
            break
        }
    }
  }
  
  // This method is static because it doesn't require self
  static func getAttkResult(_ warriorA: Warrior, _ warriorB: Warrior) -> String{
    let warriorAAttkAmt: Int = warriorA.attack();
    let warriorBBlockAmt: Int = warriorB.block();
    var dmg2WB: Int = warriorAAttkAmt - warriorBBlockAmt
    dmg2WB = dmg2WB <= 0 ? 0 : dmg2WB
    warriorB.health = warriorB.health - dmg2WB
    print("\(warriorA.name) attacks \(warriorB.name) and deals \(dmg2WB) damage")
    print("\(warriorB.name) is down to \(warriorB.health)")
    if warriorB.health <= 0{
      print("\(warriorB.name) has Died and \(warriorA.name) is Victorious!!!")
      return "Game Over"
    } else {
      return "Fight Again"
    }
  }
}


// ***** Protocols
// Protocols are like interfaces in other languages
// When a class adopts a protocol it agrees to define
// the behavior the protocol describes
// We want to give Loki the ability to teleport
protocol Teleports {
  // You define the header for a func but nothing else
  func teleport() -> String
}
 
// Now we create classes that implement Teleports
class CanTeleport : Teleports{
  func teleport() -> String{
      return "Teleports Away"
  }
}
 
class CantTeleport : Teleports{
  func teleport() -> String{
    return "Fails at Teleporting"
  }
}
 
// Now we will inherit from Warrior and add on the additonal
// ability of teleporting using our Teleports protocol
class MagicWarrior : Warrior {
  // The bigger the number the more likely the chance
  // of successfully teleporting (100 Max Value)
  var teleportChance: Int = 0
  // Add protocol functionality
  var teleportType = CanTeleport()
  
  init(_ name: String, _ health: Int, _ attkMax: Int, _ blockMax: Int, _ teleportChance: Int){
    // Call the superclass init
    super.init(name, health, attkMax, blockMax)
    self.teleportChance = teleportChance
  }
    
  // We'll inherit all properties and methods in the Warrior
  // class but we'll override block
  override func block() -> Int {
    // Generate a random value from 1 to 100
    let rndDodge = Int.random(in: 1...100)
    // Decide if teleport works based on percent assigned to teleportChance
    if rndDodge < self.teleportChance{
      print(self.name + " " + teleportType.teleport())
      return 10000
    } else {
      // Call the block method in the super class
      return super.block()
    }
  }
}
 
// Thor is more powerful then Loki so let's treat him that way
let thor = Warrior("Thor", 80, 26, 10)
// let loki = Warrior("Loki", 50, 20, 10)
 
// Now Loki while not strong has the added ability of magic
let loki = MagicWarrior("Loki", 50, 20, 10, 50)
let battle = Battle()
battle.startFight(thor, loki)

// ACCESS MODIFIERS
/* private: can't be accessed outside parent scope
 *    which means class private members can't be accessed outside class.
 *    Private class can be accessed in that file context but variable must be private or fileprivate.
 * fileprivate: scope of fileprivate is to file only. Can not be accessed outside file.
 * internal: It is by default modifier and allows access in the module context.
 * public and open: Both allow access in any module, bot open class can be subclassed
 *    whereas public class can not be subclassed outside that module.
 */

// ***** More on properties
// public, open can be accessed outside module
// But public class can not be subclassed where as open class can be
public class Cuboid {
    // Stored Properties
    // internal is by default access modifier - allows access from module itself only
    internal var height = 0.0, depth = 0.0

    // lazy stored properties
    // abcd object will only be initialized when it is first used.
    lazy var abcd = ABCD("Hello from cuboid.")
    
    // Property obervers
    var width = 0.0 {
        willSet {
            print("Width will set to \(newValue)")
        }
        didSet {
            print("Width changed from \(oldValue) t0 \(width)")
        }
    }
    
    // Typed properties
    // static can't be overridden in subclass.
    static var title = "Cuboid"
    // class can be overridden - always computed properties
    // same way class and static func works.
    class var subtitle: String { return "My Dynamic Cuboid" }
    
    // private variables
    private var _description: String = "This is a cuboid class"
    //  fileprivate --- can only be accessed in this file itself
    fileprivate var description: String {
        get {
            return self._description
        } set {
            self._description = newValue
        }
    }
    // Read-onle or get-only computed property
    var volume: Double {
        return width * height * depth
    }
    
    init(width: Double, height: Double, depth: Double) {
        self.width = width
        self.height = height
        self.depth = depth
    }
}
let fourByFiveByTwo = Cuboid(width: 4.0, height: 5.0, depth: 2.0)

fourByFiveByTwo.description = "Cuboid 4 x 5 x 2"
print(fourByFiveByTwo.description)

Cuboid.title = "CUBOID"
print(Cuboid.title)

print("the volume of fourByFiveByTwo is \(fourByFiveByTwo.volume)")
// fourByFiveByTwo.volume = 1.0 // Will give Compile Error

// Property Wrapper for wrapping value
// Can get set property decisively
@propertyWrapper
struct CamelCaps {
  private var str: String
  init() { self.str = "" }

  var wrappedValue: String {
    get {
      return str
    }
    set {
        self.str = newValue.split(separator: " ").map({ w in
            w.prefix(1).uppercased() + w.dropFirst()
        }).joined(separator: " ")
    }
  }
}

class CapsExample {
  @CamelCaps var str: String
  init() {
      self.str = "hey there! hello world"
  }
}

var cap = CapsExample()
print(cap.str)

// FINAL is not simillar to constant 
// final, private, static all 3 can not be overridden
// private is only for visibility
// static is for non-instance var/func
// final is used to reduce dynamic dispatch at runtime.
class UseFinal {
  final var val: Int = 1
  func foo() {
    self.val = 22
  }
}

var f = UseFinal()
f.foo()
print(f.val) // prints: 22


// ***** SUBSCRIPTS
// Subscripts allows us to implement custom acess of class by [] brackets.
// Subscripts overloading and static subscripts can also be defined.
class ClassRoom {
  var students = [
    ["ABSAS", "RFNDS", "AYEWAS", "SAGDKA", "SDANS"],
    ["TWEDD", "IYWEG", "IJASVD", "AYSD", "ASDK"],
    ["MAKSA", "YEIA", "QWPE", "YQHWSDA", "OIWQOIE"],
    ["EWFNCIO", "VCJBS", "MLASMD", "NAJSN", "QOWJE"],
    ["OWSM", "WEOWEE", "UIEHRN", "ASBD", "NXCV"],
  ]

  subscript(row: Int, col: Int) -> String {
    get { 
      return students[row][col]
    }
    set {
      students[row][col] = newValue
    }
  }

  subscript(name: String) -> (Int, Int)? {
    for row in 0..<students.count {
      for col in 0..<students[row].count {
        if(students[row][col] == name) {
          return (row, col)
        }
      }
    }
    return nil
  }

  static subscript(row: Int, col: Int, _: Int) -> String {
    return students[row][col]
  }
}

var classroom = ClassRoom()
print("\(classroom[1, 2]) = \(classroom.students[1][2])")

classroom[1, 2] = "AVS"
print("\n\(classroom[1, 2]) = \(classroom.students[1][2])\n")

if let pos = classroom["MAYANK"] {
  print(pos)
} else {
  print("Student 'MAYANK' Not Found")
}

print("\n\(ClassRoom[1, 2, 0])")


// ***** Extensions
extension Double {
  var kmph: Double { return self }
  var mph: Double { return self * 0.621371 }

  func kmToMile() -> Double {
    return self * 0.621371
  }

  func mileTokm() -> Double {
    return self / 0.621371
  }

  func toString() -> String {
    return String(self)
  }
}

var speed = 150.kmph
print(speed)
print(speed.mph)

var s: Double = 200.0
print(s.kmToMile())
print(s.mileTokm())
print(s.toString())

protocol Proto {
  func foo()
}

// extension can be used to provide 
// default implementation for protocols as below
extension Proto {
  func foo() {
    print("Foo")
  }
}

class Imp: Proto {}

var imp = Imp()
imp.foo()
print(imp.xyz)


// ***** GENERICS
// Generic code enables you to write flexible, reusable functions and types 
//  that can work with any type, subject to requirements that you define.
// You can write code that avoids duplication and expresses 
// its intent in a clear, abstracted manner.

// Generic Functions
func swap<T>(_ a:inout T, _ b:inout T) { (a, b) = (b, a) }
var a = 0
var b = 1
print("\(type(of: &a))")
print("a = \(a), b = \(b)")
swap(&a, &b)
print("a = \(a), b = \(b)")

func foo<T> (_ a: T) -> Void {
  print("\(a)\n")
}

foo("xyz")

// T: SomeClass or T: SomeProtocol is known as Type Constraints
// Which allows to declare types which can be used with this generic as type.
func sum<T: Numeric> (_ args:T ...) -> T {
  var sum: T = 0
  for i in args {
    sum = sum + i
  }
  return sum
}

print(sum(1.12, 123.23))
print()

func display <T: Sequence> (_ seq: T) -> Void {
  for i in seq {
    print("\(i)")
  }
}

display(1...5)
print()

display(["Hi", "Hello"])

// Generic Class
class Point<T: Numeric> {
  var x: T
  var y: T
  var z: T

  init(_ x: T, _ y: T, _ z: T) {
    self.x = x
    self.y = y
    self.z = z
  }

  func display() {
    print("X: \(x), Y: \(y), Z: \(z)")
  }

  // operator overloading using static function
  static func + (p1: Point, p2: Point) -> Point {
    return Point(p1.x + p2.x, p1.y + p2.y, p1.z + p2.z)
  }

  static func == (p1: Point, p2: Point) -> Bool {
    if (p1.x == p2.x && p1.y == p2.y && p1.z == p2.z) {
      return true
    }
    return false
  }
}

var p1 = Point<Int>(1, 2, 3)
var p2 = Point<Int>(1, 2, 3)

p1.display()
p2.display()

var p3 = p1 + p2
p3.display()

if (p1 == p2) {
  print("Points are equal.")
} else {
  print("Points are not equal.")
}


// ***** Opaque Types
// some keyword: for protocols only

// import Foundation

protocol Secret {
  associatedtype Code

  var secret: Code { get }
  static func generateSecretKey() -> Code
}

struct IntegerSecret: Secret {
  typealias Code = Int
  
  var secret: Int
  static func generateSecretKey() -> Int {
    return Int.random(in: 123891...1273981)
  }
}

struct StringSecret: Secret {
  typealias Code = String
  
  var secret: String
  static func generateSecretKey() -> String {
    return UUID().uuidString
  }
}

func createSecret() -> some Secret {
  let random = Int.random(in: 1...2)
  return i == 1 
    ? StringSecret(secret: StringSecret.generateSecretKey())
    : IntegerSecret(secret: IntegerSecret.generateSecretKey())
}

var s: some Secret = createSecret()
print(s.secret)


