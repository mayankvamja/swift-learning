var arr: [Int]! = nil

arr.append(1212)
arr[0] = 3
print(arr[0])

arr = nil
// arr.append(2)
// print(arr ?? "arr is nil")



//////////////
class X {
  var y: Y?
  init(_ y: Bool = false, _ z: Bool = false) { 
    self.y = y ? Y(z) : nil 
  }

  func infoX() { print("X INFO") }

  // CLASS Y
  class Y {
    var z: Z?
    init(_ z: Bool = false) { self.z = z ? Z() : nil }

    func infoY() { print("Y INFO") }

    // CLASS Z
    class Z {
      func infoZ() { print("Z INFO") }
    }
  }
}

var obj = X(true, true)

obj.infoX()
obj.y?.infoY()
obj.y?.z?.infoZ()

